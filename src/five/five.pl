% -*- mode: prolog -*-
% This is your main file

:- module(five, [five/1]).

five(X) :- X = 5.
